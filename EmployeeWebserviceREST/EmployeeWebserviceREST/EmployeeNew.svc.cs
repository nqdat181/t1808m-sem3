﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeNew" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeNew.svc or EmployeeNew.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEmployeeNew
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        public bool AddEmployee(EmployeeCustomer eml)
        {
            try
            {
                data.EmployeeCustomers.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployee(int idE)
        {
            try
            {
                EmployeeCustomer employeeToDelete =
                    (from employeeCustomer in data.EmployeeCustomers where employeeCustomer.EmpID== idE select employeeCustomer).Single();
                data.EmployeeCustomers.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public void DoWork()
        {
        }

        public List<EmployeeCustomer> GetProductList()
        {
            try
            {
                return (from employeeCustomer in data.EmployeeCustomers select employeeCustomer).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployee(EmployeeCustomer eml)
        {
            EmployeeCustomer employeeToModify =
                (from employeeCustomer in data.EmployeeCustomers where employeeCustomer.EmpID == eml.EmpID select employeeCustomer).Single();
            employeeToModify.Age = eml.Age;
            employeeToModify.Address = eml.Address;
            employeeToModify.FirstName = eml.FirstName;
            employeeToModify.LastName = eml.LastName;
            data.SubmitChanges();
            return true;
        }
    }
}
