﻿using System.Web;
using System.Web.Mvc;

namespace Call_EmployeeService_Consum
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
