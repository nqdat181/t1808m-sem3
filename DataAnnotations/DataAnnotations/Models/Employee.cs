﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataAnnotations.Models
{
    public class Employee
    {
        [Key]

        public int Id { get; set; }

        [DisplayName("Employee Name")]
        [Required(ErrorMessage ="Employee Name is required")]
        [StringLength(35, MinimumLength =4)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Employee Address is required")]
        [StringLength(300)]
        public string Address { get; set; }

        [Required(ErrorMessage ="Salary is required")]
        [Range(3000,1000000, ErrorMessage ="Salary must be between 3000 and 1000000")]
        public decimal Salary { get; set; }


        public string Email { get; set; }
    }
}