﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeNews.Model
{
    public class NewsItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Headline { get; set; }
        public string Subhead { get; set; }
        public string Dateline { get; set; }
        public string Image { get; set; }
    }
    public class NewsManager
    {
        public static void GetNews(String category, ObservableCollection<NewsItem> newsItems)
        {
            var allItems = getNewsItems();
            var filteredNewsItems = allItems.Where(p => p.Category == category).ToList();
            newsItems.Clear();
            filteredNewsItems.ForEach(p => newsItems.Add(p));
        }

        private static List<NewsItem> getNewsItems()
        {
            var items = new List<NewsItem>();

            items.Add(new NewsItem() { Id = 1, Category = "Financial", Headline = "Lorem Ipsum", Subhead = "doro sit amet", Dateline = "Nunc tristique nec", Image = "Assets/Images02/Financial.png" });

            items.Add(new NewsItem() { Id = 2, Category = "Financial", Headline = "Etiam ac felis viverra", Subhead = "vulputate nisl ac,aliquet nisi", Dateline = "tortor porttitor,eu fermentum ante congue", Image = "Assets/Images02/Financial.png" });

            items.Add(new NewsItem() { Id = 3, Category = "Ficancial", Headline = "Integer sed turpis erat", Subhead = "Sed quis hendrerit lorem,quis interdum dolor", Dateline = "in viverra metus facilisis sed", Image = "Assets/Images02/Financial3.png" });

            items.Add(new NewsItem() { Id = 4, Category = "Financial", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Financial4.png" });

            items.Add(new NewsItem() { Id = 5, Category = "Financial", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Financial5.png" });

            items.Add(new NewsItem() { Id = 6, Category = "Food", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Food1.png" });

            items.Add(new NewsItem() { Id = 7, Category = "Food", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Food2.png" });

            items.Add(new NewsItem() { Id = 8, Category = "Food", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Food3.png" });

            items.Add(new NewsItem() { Id = 9, Category = "Food", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Food4.png" });

            items.Add(new NewsItem() { Id = 10, Category = "Food", Headline = "Proin sem neque", Subhead = "aliquet quis ipsum tincidunt", Dateline = "Integer eleifend", Image = "Assets/Images02/Food5.png" });

            return items;
        }
    }
}
