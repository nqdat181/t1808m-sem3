﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObvervableCollectionExample.Models
{
    public class Icon
    {
        public string IconPath { get; set; }
    }
}
